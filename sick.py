import requests
import json
import numpy as np
import pygame
import time
import os

size = 128
v = np.zeros(size)

def get_val(pin):
	r = requests.get("http://192.168.0.100:8000/api/v1/analog-input/")
	return r.json()['AIN_'+pin]['Value']
	

def calibrate(pin):
	for cnt in range(size):
		v[cnt % size] = get_val(pin)
	res = np.mean(v)
	print('calibrate = %f' % res)
	return res


WHITE = (255,255,255)
BLACK = (0,0,0)
GREEN = (0,255,0)
RED = (255,0,0)
BLUE = (0,0,255)

WIDTH = 1024
HEIGHT = 768

RED_INTERVAL = 5
GREEN_INTERVAL = 5

pygame.init()
DISPLAY=pygame.display.set_mode((WIDTH,HEIGHT),0,32)

car_height = 50
car_width = 100
padding = 20

def draw_state(car_num,light):
	DISPLAY.fill(WHITE)
	pygame.draw.circle(DISPLAY,light,(100,100),70)
	for i in range(car_num):
		pygame.draw.rect(DISPLAY,BLUE,(50 + i*(car_width+padding),HEIGHT * 0.5,car_width,car_height))
	pygame.display.update()

#def partial_draw(car_num,light):

cal_A = calibrate('A')
cal_B = calibrate('F')

i = 0

detected_A = 0
detected_B = 0

cars_A = 0
cars_B = 0

cars_standing = 0
prev_cars_standing = 0

draw_state(0,RED)

start_light = time.time()
color = RED

def calc_light():
	global color
	global start_light

	if color == RED and time.time() - start_light > RED_INTERVAL:
		color = GREEN
		start_light = time.time()
		os.system("espeak -v de \"Rot\" &")
	elif color == GREEN and time.time() - start_light > GREEN_INTERVAL:
		color = RED
		start_light = time.time()
		os.system("espeak -v de \"Grün\" &")

while True:
	calc_light()
	val_A = get_val('A')
	val_B = get_val('F')
	
	if val_A <= 0.95 * cal_A:
		detected_A = 1
	elif detected_A == 1:
		detected_A = 0
		cars_A += 1
		cars_standing += 1
		print('cars_A %d' % cars_A)
	
	if val_B <= 0.95 * cal_B:
		detected_B = 1
	elif detected_B == 1:
		detected_B = 0
		cars_B += 1
		cars_standing += -1
		if color == RED:
			os.system("espeak -v el \"ίου, ίου, ίου, ίου, ίου, ίου, ίου, ίου, ίου\" &")
		print('cars_B %d' % cars_B)
	
	#if prev_cars_standing != cars_standing:
	draw_state(cars_standing,color)
		#print('cars_standing = %d' % cars_standing)


import komvos
import numpy as np
import random.rand as rnd
import time


def random_traffic(nu):
    return 1 if rnd.rand() < 0.5 else 0

#######################

antistoixisi = {"a":0,"b":1,"c":2,"d",3}
fanaria_num = 4
default_times = [30,40,30,40]
car_count_ = [13,44,3,50]
max_times = [60,80,60,80]
min_times = [7,7,7,7]

def compute_times():
    car_count[0] = komvos.a.cars_standing
    car_count[1] = komvos.b.cars_standing
    car_count[2] = komvos.c.cars_standing
    car_count[3] = komvos.d.cars_standing
    '''pairnei ena dianysma car_count me to posa amxia se kathe '''
    new_times = np.zeros(fanaria_nu)
    sum = np.sum(car_count_)
    car_count_normalized = np.multiply(4,car_count_)/sum

    for i in range(fanaria_num):
        if car_count == 0:
            new_times[i] = 0
        else:
            ypopshfio = default_times[i]*car_count_normalized[i]
            if ypopshfio < max_times[i]:
                if ypopshfio > min_times[i]:
                    new_times[i] = ypopshfio
                else:
                    new_times[i] = min_times[i]
            else:
                new_times[i] = max_times[i]
    return new_times

#####################

t = 0
prasino=0
gia_poso=0
prev = time.time()
while True:
    if time.time() - prev > 1 :
        prev = time.time()
        komvos.b.cars_standing += random_traffic()
        komvos.c.cars_standing = random_traffic() + random_traffic()
        komvos.d.cars_standing += random_traffic()
        if komvos.b.color == GREEN:
            komvos.b.cars_standing = max(komvos.b.cars_standing - 4,0)
        if komvos.c.color == GREEN:
            komvos.c.cars_standing = max(komvos.c.cars_standing - 4,0)
        if komvos.d.color == GREEN:
            komvos.d.cars_standing = max(komvos.d.cars_standing - 4,0)
        if t >= gia_poso:
            t = 0
            prasino = (prasino + 1)%4
            gia_poso =  compute_times(car_count_)[prasino]
        t += 1
    
    #Plot(car_count_,prasino)

    mainloop()
    

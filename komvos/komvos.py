from fanari import *
import drawer

a = fanari(1)
b = fanari(0)
c = fanari(0)
d = fanari(0)

def updateFanaria():
	a.update()

def mainLoop():
	updateFanaria()
	drawer.clearScreen()
	drawer.drawFanari(a, 0, int(0.5 * drawer.WIDTH), int(0.5 * drawer.HEIGHT-30))
	drawer.drawFanari(b, 1, int(0.5 * drawer.WIDTH+30), int(0.5 * drawer.HEIGHT))
	drawer.drawFanari(c, 2, int(0.5 * drawer.WIDTH), int(0.5 * drawer.HEIGHT+30))
	drawer.drawFanari(d, 3, int(0.5 * drawer.WIDTH-30), int(0.5 * drawer.HEIGHT))

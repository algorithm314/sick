import komvos
import numpy as np
import random
import time
import os


def random_traffic():
	return 1 if random.random() < 0.45 else 0

#######################


fanaria_num = 4
default_times = [10,15,15,15]
car_count_ = [13,44,3,50]
max_times = [13,13,13,13]
min_times = [2,3,2,2]

def compute_times():
	car_count_[0] = komvos.a.cars_standing
	car_count_[1] = komvos.b.cars_standing
	car_count_[2] = komvos.c.cars_standing
	car_count_[3] = komvos.d.cars_standing
	'''pairnei ena dianysma car_count me to posa amxia se kathe '''
	new_times = np.zeros(fanaria_num)
	s = np.sum(car_count_)
	car_count_normalized = np.multiply(4,car_count_)/s

	for i in range(fanaria_num):
		if car_count_ == 0:
			new_times[i] = 0
		else:
			ypopshfio = default_times[i]*car_count_normalized[i]
			if ypopshfio < max_times[i]:
				if ypopshfio > min_times[i]:
					new_times[i] = ypopshfio
				else:
					new_times[i] = min_times[i]
			else:
				new_times[i] = max_times[i]
	return new_times

#####################

t = 0
prasino=0
gia_poso=0
prev = time.time()
GREEN = (0,255,0)
RED = (255,0,0)
l_s = 3
komvos.b.cars_standing = 4
komvos.c.cars_standing = 8
komvos.d.cars_standing = 4


while True:
	if time.time() - prev > 1 :
		prev = time.time()
		komvos.b.cars_standing += random_traffic() + random_traffic()
		komvos.c.cars_standing += random_traffic()
		komvos.d.cars_standing += random_traffic()
		if komvos.b.color == GREEN:
			komvos.b.cars_standing = max(komvos.b.cars_standing - l_s,0)
		if komvos.c.color == GREEN:
			komvos.c.cars_standing = max(komvos.c.cars_standing - l_s,0)
		if komvos.d.color == GREEN:
			komvos.d.cars_standing = max(komvos.d.cars_standing - l_s,0)
		flag = 0
		if prasino == 0 and komvos.a.cars_standing == 0:
			flag = 1
		elif prasino == 1 and komvos.b.cars_standing == 0:
			flag = 1
		elif prasino == 2 and komvos.c.cars_standing == 0:
			flag = 1
		elif prasino == 3 and komvos.d.cars_standing == 0:
			flag = 1
		#print('cars_standing_a %d' % komvos.a.cars_standing)
		#print('cars_standing_b %d' % komvos.b.cars_standing)
		#print('cars_standing_c %d' % komvos.c.cars_standing)
		#print('cars_standing_d %d' % komvos.d.cars_standing)
		if t >= gia_poso or flag == 1:
			t = 0
			prasino = (prasino + 1)%4
			if prasino == 0 and komvos.a.cars_standing != 0:
				os.system("espeak -v en \"RED\" & ")
			elif prasino == 1 and komvos.a.cars_standing != 0:
				os.system("espeak -v en \"GREEN\" &")
			komvos.a.color = komvos.b.color = komvos.c.color = komvos.d.color = RED 
			if prasino == 0:
				komvos.a.color = GREEN
			if prasino == 1:
				komvos.b.color = GREEN
			if prasino == 2:
				komvos.c.color = GREEN
			if prasino == 3:
				komvos.d.color = GREEN
			gia_poso =  compute_times()[prasino]
		flag = 0
		t += 1

	#Plot(car_count_,prasino)

	komvos.mainLoop()


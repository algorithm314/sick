from fanari import *
import pygame

WIDTH = 1024
HEIGHT = 768
WHITE = (200,200,150)
BLACK = (0,0,0)
BLUE = (65,65,190)
GRAY = (65,65,65)
car_height = 13
car_width = 25
padding = 20

pygame.init()

DISPLAY=pygame.display.set_mode((WIDTH,HEIGHT),0,32)
DISPLAY.fill(WHITE)

def clearScreen():
	DISPLAY.fill(WHITE)

def drawFanari(fan, orientation, x, y):
	
	xtemp = x
	ytemp = y
	if orientation==0:
		pygame.draw.circle(DISPLAY, fan.color, (xtemp, ytemp), 15)
		for i in range(fan.cars_standing):
			ytemp = ytemp - car_width - 10
			pygame.draw.circle(DISPLAY, BLUE, (xtemp, ytemp), car_height)
			#pygame.draw.rect(DISPLAY, BLUE, (xtemp, ytemp, car_width, car_height))
	if orientation==1:
		pygame.draw.circle(DISPLAY, fan.color, (xtemp, ytemp), 15)
		for i in range(fan.cars_standing):
			xtemp = xtemp + car_width + 10
			pygame.draw.circle(DISPLAY, BLUE, (xtemp, ytemp), car_height)
			#pygame.draw.rect(DISPLAY, BLUE, (xtemp, ytemp, car_width, car_height))
	if orientation==2:
		pygame.draw.circle(DISPLAY, fan.color, (xtemp, ytemp), 15)
		for i in range(fan.cars_standing):
			ytemp = ytemp + car_width + 10
			pygame.draw.circle(DISPLAY, BLUE, (xtemp, ytemp), car_height)
			#pygame.draw.rect(DISPLAY, BLUE, (xtemp, ytemp, car_width, car_height))
	if orientation==3:
		pygame.draw.circle(DISPLAY, fan.color, (xtemp, ytemp), 15)
		for i in range(fan.cars_standing):
			xtemp = xtemp - car_width - 10
			pygame.draw.circle(DISPLAY, BLUE, (xtemp, ytemp), car_height)
			#pygame.draw.rect(DISPLAY, BLUE, (xtemp, ytemp, car_width, car_height))
	pygame.display.update()

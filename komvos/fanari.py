import numpy as np
import os
import time
import sensorGetter

class fanari(object):
	def __init__(self, inp):
		self.size=128
		self.v=np.zeros(self.size)
		self.RED_INTERVAL = 5
		self.GREEN_INTERVAL = 5
		self.detected_A = 0
		self.detected_B = 0
		self.cars_A = 0
		self.cars_B = 0
		self.THR = 0.97
		self.GREEN = (0,255,0)
		self.RED = (255,0,0)
		self.cars_standing = 0
		self.prev_cars_standing = 0
		self.start_light = time.time()
		self.color = self.RED
		self.val_A = 0
		self.val_B = 0
		self.inputFromSensors = inp
		if self.inputFromSensors==1:
			self.cal_A = self.calibrate('A')
			self.cal_B = self.calibrate('F')
	
	def calibrate(self, pin):
		for cnt in range(self.size):
			self.v[cnt % self.size] = sensorGetter.getValue(pin)
		res = np.mean(self.v)
		print('calibrate = %f' %res)
		return res
	
	def update(self):
		if self.inputFromSensors==1:
			self.val_A = sensorGetter.getValue('A')
			self.val_B = sensorGetter.getValue('F')
			if self.val_A <= self.THR * self.cal_A:
				self.detected_A = 1
			elif self.detected_A == 1:
				self.detected_A = 0
				self.cars_A += 1
				self.cars_standing += 1
				print('cars_A %d' % self.cars_A)
			
			if self.val_B <= self.THR * self.cal_B:
				self.detected_B = 1
			elif self.detected_B == 1:
				self.detected_B = 0
				self.cars_B += 1
				#self.cars_standing = max(self.cars_standing-1,0)
				if self.cars_standing > 0:
					self.cars_standing -= 1
					if self.color == self.RED:
						os.system('espeak -v el \"ίου, ίου, ίου, ίου, ίου, ίου, ίου\" &')
				print('cars_B %d' % self.cars_B)
				

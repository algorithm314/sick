import numpy as np
fanaria_nu = 4
default_times = [30,40,30,40]
car_count_ = [13,44,3,50]
max_times = [60,80,60,80]
min_times = [7,7,7,7]
sum = np.sum(car_count)
car_count_normalized = np.multiply(4,car_count)/sum


def compute_times(car_count):
    '''pairnei ena dianysma car_count me to posa amxia se kathe fanari '''
    new_times = np.zeros(fanaria_nu)
    for i in range(fanaria_nu):
        if car_count == 0:
            new_times[i] = 0
        else :
            ypopshfio = default_times[i]*car_count_normalized[i]
            if ypopshfio < max_times[i]:
                if ypopshfio > min_times[i]:
                    new_times[i] = ypopshfio
                else:
                    new_times[i] = min_times[i]
            else:
                new_times[i] = max_times[i]
    return new_times


compute_times(car_count_)




